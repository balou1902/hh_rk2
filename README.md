clear all
close all
clc

% What : Hodgking Huxley neuron model
% When : September 27th, 2017

% initialisation variables
vna = 55;
vk = -77;
vl = -65; % mV
gna = 40; % mS / cm2
gk = 36;
gl = 0.3;
cm = 1; %microFarad / cm2
I = 1.6;

% Parametres simulation
dt = 0.0313;
Tsimulation = 1500 ; % ms
t=0:dt:Tsimulation;

% initialisation (CI)
v(1) = vl;
m(1) = 0.0498;
n(1) = 4.5379e-04;
h(1) = 0.6225;

for i=1:(length(t)-1)
    
    kv = (1/cm)*(I   -   gk*(v(i)-vk)*n(i)^4   -   gna*m(i)^3*h(i)*(v(i)-vna)  -  gl*(v(i)-vl) );
    km = 0.182.*(v(i)+35)./(1-exp(-(v(i)+35)./9)).*(1-m(i))    +   0.124.*(v(i)+35)./(1-exp((v(i)+35)/9)).*m(i);
    kn = 0.02.*(v(i)-25)./(1-exp(-(v(i)-25)./9)).*(1-n(i))     +   0.002.*(v(i)-25)./(1-exp((v(i)-25)./9)).*n(i);
    kh = 0.25*exp(-(v(i)+90)/12).*(1-h(i))                         -   (0.25* exp((v(i)+62)/6))./exp((v(i)+90)/12).*h(i);

    % intermediate value
    v1 = v(i) + kv*dt/2;
%     m1 = m(i) + km*dt/2;
%     n1 = n(1) + kn*dt/2;
%     h1 = h(1) + kh*dt/2;
    
    k2_v = (1/cm)*(I   -   gk*(v1-vk)*n(i)^4   -   gna*m(i)^3*h(i)*(v1-vna)  -  gl*(v1-vl) );
    k2_m = 0.182.*(v1+35)./(1-exp(-(v1+35)./9)).*(1-m(i))    +   0.124.*(v1+35)./(1-exp((v1+35)/9)).*m(i);
    k2_n = 0.02.*(v1-25)./(1-exp(-(v1-25)./9)).*(1-n(i))     +   0.002.*(v1-25)./(1-exp((v1-25)./9)).*n(i);
    k2_h = 0.25*exp(-(v1+90)/12).*(1-h(i))                         -   (0.25* exp((v1+62)/6))./exp((v1+90)/12).*h(i);

    v(i+1) = v(i) + k2_v*dt;
    m(i+1) = m(i) + k2_m*dt;
    n(i+1) = n(i) + k2_n*dt;
    h(i+1) = h(i) + k2_h*dt;
end

%%
% To see the evolution of the parameters n, m, h and v
figure,
for i=1:(length(t)-1)
    subplot(4,1,1)
    hold on
    scatter(i,n(i),'*')
    grid on
    title('n')
    pause(0.00001)
    
    subplot(4,1,2)
    hold on
    scatter(i,m(i),'*')
    grid on
    title('m')
    pause(0.00001)
    
    subplot(4,1,3)
    hold on
    scatter(i,h(i),'*')
    grid on
    title('h')
    pause(0.00001)
        
    subplot(4,1,4)
    hold on
    scatter(i,v(i),'*')
    grid on
    title('v')
    pause(0.00001)
end

%%


figure,
subplot(2,1,1)
plot(t,n)
hold on
plot(t,m)
plot(t,h)
legend('n','m','h','location','southwest')
grid on
subplot(2,1,2)
plot(t,v)
grid on
